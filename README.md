The Garden Gnome’s specialty is upgrading or totally redesigning residential landscapes. We consult with the customer then design a landscape based on the customer’s wants and needs. We pride ourselves on putting the right plant in the right place. Call Us Today!

Address: 29251 Chandler Trce, Wesley Chapel, FL 33545, USA

Phone: 813-789-9616
